#!/bin/bash

hook_dir=$(git rev-parse --show-toplevel)/.git/hooks

if [[ -f "${hook_dir}/pre-commit" ]]; then
	echo "File ${hook_dir}/pre-commit already exists. Exiting..."
	exit 1
fi

cp -vi utils/pre-commit "${hook_dir}/"
