// This file is for local development. It will be overriden by environment
// variables with prefix SISIYA_UI_ in the nginx container by the entrypoint
// script.
window.injectedEnv = {
  SISIYA_UI_API_URL: "http://127.0.0.1:19292",
  SISIYA_UI_VERSION: "dev",
};
