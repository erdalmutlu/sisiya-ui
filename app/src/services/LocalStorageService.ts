import * as crypto from "crypto-js";

export class LocalStorageService {
  public static set(key: string, data: string) {
    let crypted_data = crypto.AES.encrypt(data, import.meta.env.VITE_SECRET).toString();
    localStorage.setItem(key, crypted_data);
  }

  public static get(key: string): string {
    let encryptedText = localStorage.getItem(key);
    if (encryptedText) {
      let bytes = crypto.AES.decrypt(localStorage.getItem(key), import.meta.env.VITE_SECRET);
      let originalText = bytes.toString(crypto.enc.Utf8);
      return originalText;
    }
    return "";
  }

  public static remove(key: string) {
    localStorage.removeItem(key);
  }
}
