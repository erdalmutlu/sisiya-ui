import jwtDecode, { JwtPayload } from "jwt-decode";
import { LocalStorageService } from "./LocalStorageService";
import { setLoggedIn, setCurrentUser } from "../App";

export class AuthenticationService {
  public static setCurrentUser() {
    console.log("services/AuthenticationService.ts:setCurrentUser:");
    let access_token = LocalStorageService.get("access_token");
    if (!access_token) {
      console.log("services/AuthenticationService.ts:setCurrentUser: no access_token found in the local storage");
      return;
    }
    try {
      console.log("services/AuthenticationService.ts:setCurrentUser: found access_token in the local storage");
      let payload = jwtDecode<JwtPayload>(access_token);
      console.log("services/AuthenticationService.ts:setCurrentUser: payload: ", payload);
      setCurrentUser(payload);
      setLoggedIn(true);
    } catch (error) {
      this.logout();
      console.log(error);
    }
  }

  public static login(email: string, password: string) {
    console.log(`services/AuthenticationService.ts:login: email=${email}`);
  }

  public static logout() {
    console.log("services/AuthenticationService.ts:logout");
    LocalStorageService.remove("access_token");
    LocalStorageService.remove("refresh_token");
    setLoggedIn(false);
    setCurrentUser({});
  }
}
