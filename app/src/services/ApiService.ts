import http from "./AxiosCommon";
import axios from "axios";
import { AxiosRequestConfig } from "axios";
import { LocalStorageService } from "./LocalStorageService";

// https://dev.to/serifcolakel/building-a-resilient-axios-api-service-with-error-handling-and-notifications-4lm6

export class ApiService {
  private url: string;
  private secure: boolean;
  private headers: any;

  constructor(url: string, secure: boolean = true) {
    console.log(`services/ApiService.tsx:constructor: ${url}`);
    this.url = url;
    this.secure = secure;
    this.headers = { "Content-type": "application/json" };
    if (this.secure) {
      this.headers.Authorization = "Bearer " + LocalStorageService.get("access_token");
    }
  }

  public getEndpoint() {
    return this.url;
  }

  public async getObjects(object: string, params?: any) {
    return http.get(object);
  }

  public async createObject(object: string, data: any) {
    return http.post(object, data);
  }

  public async get(url: string, data: any = "") {
    // const headers = { "Content-type": "application/json" }
    // if (this.secure) {
    //   headers.Authorization = "Bearer " + LocalStorageService.get("access_token");
    // }

    const http_a = axios.create({ baseURL: this.url, headers: this.headers });

    return http_a.get(url, data);
  }
}
