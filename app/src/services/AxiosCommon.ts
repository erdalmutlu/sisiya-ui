import axios from "axios";

console.log(`services/AxiosCommon.tsx:create: baseURL: ${window.injectedEnv.SISIYA_UI_API_URL}`);

export default axios.create({
  // baseURL: import.meta.env.VITE_SERVER_URL,
  baseURL: window.injectedEnv.SISIYA_UI_API_URL,
  headers: {
    "Content-type": "application/json",
  },
});
