import type { Component } from "solid-js";
import HeaderNavigation from "../components/HeaderNavigation";
import Sidebar from "../components/Sidebar";
import AppVersion from "../components/AppVersion";

const DashboardLayout: Component<{}> = (props: any) => {
  return (
    <>
      <main id="main">
        <HeaderNavigation></HeaderNavigation>
        <section class="container-fluid">
          <nav class="mb-4 pb-2 border-bottom" aria-label="breadcrumb">
            <ol class="breadcrumb">
              <li class="breadcrumb-item">
                <a href="/">
                  <i class="ri-home-line align-bottom me-1"></i> Dashboard{" "}
                </a>
              </li>
              {/* <li class="breadcrumb-item active" aria-current="page">Blank Page</li>*/}
            </ol>
          </nav>

          {props.children}

          <footer class="footer m0">
            <AppVersion />
          </footer>
          <div class="menu-overlay-bg"></div>
          <div
            class="modal fade"
            id="exampleModal"
            tabindex="-1"
            aria-labelledby="exampleModalLabel"
            aria-hidden="true"
          >
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">
                    Modal title
                  </h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body"> Here goes modal body content </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">
                    Close
                  </button>
                  <button type="button" class="btn btn-primary">
                    Save changes
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div
            class="offcanvas offcanvas-end"
            tabindex="-1"
            id="offcanvasExample"
            aria-labelledby="offcanvasExampleLabel"
          >
            <div class="offcanvas-header">
              <h5 class="offcanvas-title" id="offcanvasExampleLabel">
                Offcanvas
              </h5>
              <button
                type="button"
                class="btn-close text-reset"
                data-bs-dismiss="offcanvas"
                aria-label="Close"
              ></button>
            </div>
            <div class="offcanvas-body">
              <div>
                {" "}
                Some text as placeholder. In real life you can have the elements you have chosen. Like, text, images,
                lists, etc.{" "}
              </div>
              <div class="dropdown mt-3">
                <button
                  class="btn btn-secondary dropdown-toggle"
                  type="button"
                  id="dropdownMenuButton"
                  data-bs-toggle="dropdown"
                >
                  {" "}
                  Dropdown button{" "}
                </button>
                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <li>
                    <a class="dropdown-item" href="#">
                      Action
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="#">
                      Another action
                    </a>
                  </li>
                  <li>
                    <a class="dropdown-item" href="#">
                      Something else here
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div
            class="offcanvas offcanvas-end"
            tabindex="-1"
            id="offcanvasNotifications"
            aria-labelledby="offcanvasNotificationsLabel"
          >
            <div class="offcanvas-header">
              <h5 class="offcanvas-title" id="offcanvasNotificationsLabel">
                Notifications
              </h5>
              <button
                type="button"
                class="btn-close text-reset"
                data-bs-dismiss="offcanvas"
                aria-label="Close"
              ></button>
            </div>
            <div class="offcanvas-body">
              <div class="d-flex justify-content-start align-items-start p-3 rounded bg-light mb-3">
                <div class="position-relative mt-1 ">
                  <picture class="avatar avatar-sm">
                    <img
                      src="./assets/images/users/profile-icon.png"
                      alt="HTML Bootstrap Admin Template by Pixel Rocket"
                    />
                  </picture>
                  <span class="dot bg-success avatar-dot border-light dot-sm"></span>
                </div>
                <div class="ms-4">
                  <p class="fw-bolder mb-1">John Jackson</p>
                  <p class="text-muted small mb-0">
                    Just sent over regional sales. If you can let me know by the end...
                  </p>
                  <span class="fs-xs fw-bolder text-muted text-uppercase">5 mins ago</span>
                </div>
              </div>
              <div class="d-flex justify-content-start align-items-start p-3 rounded bg-light mb-3">
                <div class="position-relative mt-1 ">
                  <picture class="avatar avatar-sm">
                    <img
                      src="./assets/images/users/profile-icon.png"
                      alt="HTML Bootstrap Admin Template by Pixel Rocket"
                    />
                  </picture>
                  <span class="dot bg-success avatar-dot border-light dot-sm"></span>
                </div>
                <div class="ms-4">
                  <p class="fw-bolder mb-1">Peter Smith</p>
                  <p class="text-muted small mb-0">Hi Rob, can we setup a meeting for tomorrow around 2pm...</p>
                  <span class="fs-xs fw-bolder text-muted text-uppercase">30 mins ago</span>
                </div>
              </div>
              <div class="d-flex justify-content-start align-items-start p-3 rounded bg-light mb-3">
                <div class="position-relative mt-1 ">
                  <picture class="avatar avatar-sm">
                    <img
                      src="./assets/images/users/profile-icon.png"
                      alt="HTML Bootstrap Admin Template by Pixel Rocket"
                    />
                  </picture>
                  <span class="dot bg-danger avatar-dot border-light dot-sm"></span>
                </div>
                <div class="ms-4">
                  <p class="fw-bolder mb-1">Helen Lewis</p>
                  <p class="text-muted small mb-0">
                    Need to arrange for this year's Office lisences. Have to add two team licenses...
                  </p>
                  <span class="fs-xs fw-bolder text-muted text-uppercase">43 mins ago</span>
                </div>
              </div>
              <a href="#" class="btn btn-outline-secondary w-100 mt-4" role="button">
                View all notifications
              </a>
            </div>
          </div>
        </section>
      </main>
      <Sidebar></Sidebar>
    </>
  );
};

export default DashboardLayout;
