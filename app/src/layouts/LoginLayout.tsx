import type { Component } from "solid-js";

console.log("layouts/LoginForm.tsx");

const LoginLayout: Component<{}> = (props: any) => {
  return (
    <section class="d-flex justify-content-center align-items-start vh-100 py-5 px-3 px-md-0">{props.children}</section>
  );
};

export default LoginLayout;
