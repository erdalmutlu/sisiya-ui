import { lazy } from "solid-js";
import { Component, createSignal, onMount } from "solid-js";
import { Router, Route } from "@solidjs/router";

import { Toaster } from "solid-toast";

// import Dashboard from "./pages/Dashboard";
const Dashboard = lazy(() => import("./pages/Dashboard"));
import Login from "./pages/Login";
import { AuthenticationService } from "./services/AuthenticationService";
// import Services from "./pages/Services";
const Services = lazy(() => import("./pages/Services"));

export const [currentUser, setCurrentUser] = createSignal({});
export const [loggedIn, setLoggedIn] = createSignal(false);
export const [uiState, setUiState] = createSignal({
  sidebarOpened: true,
  searchActive: false,
  notificationsPanelActive: false,
});

function App(props) {
  onMount(() => {
    console.log("App.tsx:onMount: calling AuthenticationService.setCurrentUser");
    AuthenticationService.setCurrentUser();
  });
  return (
    <>
      <Router>
        <Route path="/login" component={Login} />
        <Route path="/services" component={Services} />
        <Route path="/" component={Dashboard} />
      </Router>
      <Toaster position="bottom-center" gutter={8} />
    </>
  );
}

export default App;
