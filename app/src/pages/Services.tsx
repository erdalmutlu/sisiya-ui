import { useParams, useSearchParams } from "@solidjs/router";
import { createEffect, createResource, createSignal, For, onMount } from "solid-js";
import DashboardLayout from "../layouts/DashboardLayout";
import LoginLayout from "../layouts/LoginLayout";
import http from "../services/AxiosCommon";

console.log("pages/Services.tsx: loaded");

function Services() {
  const [params, setParams] = useSearchParams();
  const [system, setSystem] = createSignal({});
  const [services, setServices] = createSignal([]);

  createEffect(() => {});
  onMount(() => {
    fetchServices();
  });

  function fetchServices() {
    http.get("services?system_id=" + params.system_id).then(async (resp) => {
      setSystem(await resp.data.result.system);
      console.log(await resp.data.result.system.services);
      setServices(await resp.data.result.system.services);
    });
  }

  return (
    <DashboardLayout>
      <div class="card mb-4">
        <div class="card-header">
          <h6 class="card-title">{system().name}</h6>
        </div>
        <div class="card-body">
          <table class="table">
            <thead>
              <tr>
                <th scope="col">Service</th>
                <th scope="col">Status</th>
                <th scope="col">description</th>
                <th scope="col">Update Time</th>
                <th scope="col">Status Change Time</th>
                <th scope="col">Status Changed since</th>
              </tr>
            </thead>
            <tbody>
              <For each={system().services}>
                {(service, i) => (
                  <tr>
                    <th>{service.service.name}</th>
                    <td>
                      <img
                        src={`/src/assets/${service.service.status}.png`}
                        class="card-img-top  w-25 rounded mx-auto"
                      />{" "}
                    </td>
                    <td>{service.service_data[0].description}</td>
                    <td>{service.service_data[0].update_date}</td>
                    <td>{service.service_data[0].update_date}</td>
                    <td>{service.service_data[0].update_date - service.service_data[0].create_date}</td>
                  </tr>
                )}
              </For>
            </tbody>
          </table>
        </div>
      </div>
    </DashboardLayout>
  );
}

export default Services;
