import { Component, createEffect } from "solid-js";
import LoginForm from "../components/LoginForm";
import LoginLayout from "../layouts/LoginLayout";
import { useNavigate } from "@solidjs/router";
import { currentUser, loggedIn } from "../App";

const Login: Component = () => {
  const navigator = useNavigate();

  createEffect(() => {
    if (loggedIn()) {
      navigateToDashboard();
    }
  });

  function navigateToDashboard() {
    navigator("/", { replace: true });
  }

  return (
    <LoginLayout>
      <LoginForm></LoginForm>
    </LoginLayout>
  );
};

export default Login;
