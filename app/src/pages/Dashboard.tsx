import { useNavigate } from "@solidjs/router";
import { createEffect } from "solid-js";
import { loggedIn } from "../App";
import DashboardLayout from "../layouts/DashboardLayout";

import SystemsView from "../components/SystemsView";

console.log("pages/Dashboard.tsx: loaded");

function Dashboard() {
  const navigator = useNavigate();

  createEffect(() => {
    if (!loggedIn()) {
      console.log("pages/Dashboard.tsx:createEffect: User is not logged in, redirecting to /login");
      navigator("/login", { replace: true });
    }
  });

  if (loggedIn()) {
    return (
      <DashboardLayout>
        <SystemsView></SystemsView>
      </DashboardLayout>
    );
  }
}

export default Dashboard;
