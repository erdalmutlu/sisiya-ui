import { Component, createSignal, onMount } from "solid-js";

/*
const AppVersion_v1: Component<{}> = () => {
  const [version, setVersion] = createSignal<string>("Loading...");
  const year = new Date().getFullYear();

  onMount(async () => {
    try {
      const response = await fetch("/assets/version.txt");
      if (!response.ok) {
        // throw new Error(`Failed to fetch version: ${response.statusText}`);
        setVersion("F.E.R");
      } else {
        const text = await response.text();
        setVersion(text.trim());
      }
    } catch (error) {
      console.error("Error fetching version file:", error);
      setVersion("E.E.R");
    }
  });

  return (
    <div>
      <p class="small text-muted m-0">
        {year} © SisIYA: {version()}
      </p>
    </div>
  );
};
*/

const AppVersion: Component<{}> = () => {
  const year = new Date().getFullYear();

  return (
    <div>
      <p class="small text-muted m-0">
        {year} © SisIYA: {window.injectedEnv.SISIYA_UI_VERSION}
      </p>
    </div>
  );
};

export default AppVersion;
