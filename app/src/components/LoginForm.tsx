import { Component } from "solid-js";
import { createForm } from "@felte/solid";
import { ApiService } from "../services/ApiService";
import { LocalStorageService } from "../services/LocalStorageService";
import { AuthenticationService } from "../services/AuthenticationService";
import AppVersion from "../components/AppVersion";

function LoginForm(props: props) {
  console.log(`components/LoginForm.tsx:LoginForm: ${window.injectedEnv.SISIYA_UI_API_URL}`);
  const api = new ApiService(window.injectedEnv.SISIYA_UI_API_URL);
  const { form } = createForm({
    onSubmit: async (values) => {
      /* call to an api */
      api
        .createObject("api/v1/auth/token", {
          email: values.email,
          password: values.password,
        })
        .then((resp) => {
          // console.log(resp)
          LocalStorageService.set("access_token", resp.data.access_token);
          LocalStorageService.set("refresh_token", resp.data.refresh_token);
          AuthenticationService.setCurrentUser();
        });
    },
  });

  return (
    <>
      <div class="d-flex flex-column w-100 align-items-center">
        <a href="/" class="d-table mt-5 mb-4 mx-auto">
          <div class="d-flex align-items-center justify-content-center">
            <img src="/assets/favicon.ico"></img>
            <span class="fw-bold fs-3 text-white">SisIYA</span>
          </div>
        </a>
        <div class="shadow-lg rounded p-4 p-sm-5 bg-white form">
          <h5 class="fw-bold text-muted">Login</h5>
          <form class="mt-4" use:form>
            <div class="form-group">
              <label class="form-label form-label-light" for="login-email">
                Email address
              </label>
              <input
                type="email"
                class="form-control form-control-light"
                id="login-email"
                placeholder="name@email.com"
                name="email"
              />
            </div>
            <div class="form-group">
              <label
                for="login-password"
                class="form-label form-label-light d-flex justify-content-between align-items-center"
              >
                Password
              </label>
              <input
                type="password"
                class="form-control form-control-light"
                id="login-password"
                placeholder="Enter your password"
                name="password"
              />
            </div>
            <button type="submit" class="btn btn-primary d-block w-100 my-4">
              Login
            </button>
          </form>
        </div>
        <footer class="footer m0">
          <AppVersion />
        </footer>
      </div>
    </>
  );
}

export default LoginForm;
