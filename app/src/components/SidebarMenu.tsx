import { useNavigate } from "@solidjs/router";

function SidebarMenu(props) {
  const navigator = useNavigate();

  return (
    <ul class="list-unstyled mb-6 aside-menu">
      <li class="menu-section">Menu</li>
      <li class="menu-item">
        <a class="d-flex align-items-center menu-link" role="button" onClick={() => navigator("/", { replace: true })}>
          <i class="ri-home-4-line me-3"></i>
          <span>Dashboard</span>
        </a>
      </li>
    </ul>
  );
}

export default SidebarMenu;
