import { createEffect, createSignal, For, Show } from "solid-js";
import { ApiService } from "../services/ApiService";

import Box from "@suid/material/Box";

function SystemsView(props: any) {
  // const api = new ApiService("http://127.0.0.1:19292");
  const [systems, setSystems] = createSignal([]);
  const api = new ApiService(window.injectedEnv.SISIYA_UI_API_URL);
  createEffect(() => {
    // api.getObjects("api/v1/systems").then((resp) => {
    //   // get system status info as well
    //   setSystems(resp.data);
    // });
    api.get("api/v1/systems").then((resp) => {
      setSystems(resp.data);
    });
  });

  return (
    <div class="card mb-4">
      <div class="card-header align-items-center d-flex my-auto">
        <h6 class="card-title m-0">Systems</h6>
      </div>
      <Show when={systems().length} fallback={<div class="card-body">No systems.</div>}>
        <div class="card-body">
          <Box
            sx={{
              display: "flex",
              flexWrap: "wrap",
              "& > :not(style)": {
                m: 1,
                width: 160,
                height: 128,
              },
            }}
            justifyContent="center"
          >
            <For each={systems()}>
              {(system, i) => (
                <div class="card">
                  <img
                    src={`/assets/images/app/${system.status_image}`}
                    class="w-50 mx-auto"
                    data-toggle="tooltip"
                    data-placement="bottom"
                    title={`${system.name}: ${system.status_description}`}
                  />
                </div>
              )}
            </For>
          </Box>
        </div>
      </Show>
    </div>
  );
}

export default SystemsView;
