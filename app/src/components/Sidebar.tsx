import { Component } from "solid-js";
import { currentUser } from "../App";
import SidebarMenu from "./SidebarMenu";
import AppVersion from "./AppVersion";

const Sidebar: Component<{}> = (props) => {
  return (
    <aside class="aside bg-dark-700">
      <div class="simplebar-wrapper">
        <div data-pixr-simplebar>
          <div class="pb-6 pb-sm-0 position-relative">
            <div class="cursor-pointer close-menu me-4 text-primary-hover transition-color disable-child-pointer position-absolute end-0 top-0 mt-3 pt-1 d-xl-none">
              <i class="ri-close-circle-line ri-lg align-middle me-n2"></i>
            </div>
            <div class="d-flex justify-content-center align-items-center py-3">
              <a class="m-0" href="/">
                <div class="d-flex align-items-center justify-content-center">
                  <img src="/assets/favicon.ico"></img>
                  <span class="fw-bold fs-3 text-white">SisIYA</span>
                </div>
              </a>
            </div>
            <div class="border-bottom pt-3 pb-5 mb-6 d-flex flex-column align-items-center">
              <div class="position-relative">
                <picture class="avatar avatar-profile">
                  <img class="avatar-profile-img" src="/assets/images/users/profile-icon.png" alt="Profile icon" />
                </picture>
                <span class="dot bg-success avatar-dot"></span>
              </div>
              <p class="mb-0 mt-3 text-white">{currentUser().username}</p>
              <small>{currentUser().email}</small>
            </div>
            <SidebarMenu></SidebarMenu>
            <footer class="footer m0">
              <AppVersion />
            </footer>
          </div>
        </div>
      </div>
    </aside>
  );
};

export default Sidebar;
