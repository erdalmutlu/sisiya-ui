import { createEffect } from "solid-js";
import { uiState, setUiState } from "../App";
import { AuthenticationService } from "../services/AuthenticationService";

import Drawer from "@suid/material/Drawer";
import toast from "solid-toast";

function HeaderNavigation(props) {
  createEffect(() => {
    setSidebarState(uiState().sidebarOpened);
  });

  function setSidebarState(openState: boolean) {
    let body = document.querySelector("body");
    if (uiState().sidebarOpened != openState) {
      setUiState({ ...uiState(), sidebarOpened: openState });
    }
    openState ? body?.classList.remove("menu-hidden") : body?.classList.add("menu-hidden");
  }

  function setSearchBarState(openState: boolean) {
    let body = document.querySelector("body");
    let searchBar = document.querySelector(".navbar-search");
    if (uiState().searchActive != openState) {
      setUiState({ ...uiState(), searchActive: openState });
    }
    openState ? body?.classList.add("search-active") : body?.classList.remove("search-active");
    openState ? searchBar?.classList.remove("d-none") : searchBar?.classList.add("d-none");
  }
  function setNotificationsCanvasState(openState: boolean) {
    console.log("ddd");
    setUiState({ ...uiState(), notificationsPanelActive: openState });
    toast.success("Toast launched successfully!");
  }

  return (
    <nav class="navbar navbar-expand-lg navbar-light border-0 py-0 fixed-top bg-dark-800">
      <div class="container-fluid">
        <div class="d-flex justify-content-between align-items-center flex-grow-1 navbar-actions">
          <div
            class="cursor-pointer me-4 text-primary-hover transition-color disable-child-pointer"
            onClick={() => setSidebarState(!uiState().sidebarOpened)}
          >
            <i class="fa-solid fa-bars"></i>
          </div>
          <div class="d-flex align-items-center">
            <button class="btn-icon btn-hover-dark btn-search me-2" onClick={() => setSearchBarState(true)}>
              <i class="fa-solid fa-magnifying-glass text-white"></i>
            </button>

            <div class="navbar-search d-none">
              <div class="input-group mb-3 h-100">
                <span class="input-group-text px-4 bg-transparent">
                  <i class="ri-search-line ri-lg"></i>
                </span>
                <input
                  type="text"
                  class="form-control text-body bg-transparent border-0"
                  placeholder="Enter your search terms..."
                />
                <span
                  class="input-group-text px-4 cursor-pointer disable-child-pointer close-search bg-transparent"
                  onClick={() => setSearchBarState(false)}
                >
                  <i class="fa-solid fa-circle-xmark"></i>
                </span>
              </div>
            </div>

            <a
              class="btn-icon btn-hover-dark position-relative p-2 disable-child-pointer"
              data-bs-toggle="offcanvas"
              role="button"
              onClick={() => setNotificationsCanvasState(true)}
            >
              <i class="fa-solid fa-bell text-white"></i>
              <span class="badge bg-primary text-white position-absolute top-0 end-0">3</span>
            </a>
            <a
              class="btn-icon btn-hover-dark position-relative p-2 disable-child-pointer"
              role="button"
              onClick={() => AuthenticationService.logout()}
            >
              <i class="fa-solid fa-right-from-bracket text-white"></i>
            </a>
          </div>
        </div>
      </div>
      <Drawer
        anchor="right"
        open={uiState().notificationsPanelActive}
        sx={{ zIndex: 9999 }}
        onClose={setNotificationsCanvasState(false)}
      >
        <div class="offcanvas-header">
          <h5 class="offcanvas-title" id="offcanvasNotificationsLabel">
            Notifications
          </h5>
          <button
            type="button"
            class="btn-close text-reset"
            onClick={() => setNotificationsCanvasState(false)}
          ></button>
        </div>
        <div class="offcanvas-body">
          <div class="d-flex justify-content-start align-items-start p-3 rounded bg-light mb-3">
            <div class="position-relative mt-1 ">
              <picture class="avatar avatar-sm">
                <img src="/assets/favicon.ico" />
              </picture>
              <span class="dot bg-success avatar-dot border-light dot-sm"></span>
            </div>
            <div class="ms-4">
              <p class="fw-bolder mb-1">SisIYA Team</p>
              <p class="text-muted small mb-0">Have fun!</p>
              <span class="fs-xs fw-bolder text-muted text-uppercase">30 mins ago</span>
            </div>
          </div>
        </div>
      </Drawer>
    </nav>
  );
}

export default HeaderNavigation;
