## Usage

Use either npm or pnpm.

Those templates dependencies are maintained via [pnpm](https://pnpm.io) via `pnpm up -Lri`.

This is the reason you see a `pnpm-lock.yaml`. That being said, any package manager will work.
This file can be safely be removed once you clone a template.

```
# or pnpm install or yarn install
pnpm install
```

## Available Scripts

In the project directory, you can run:

### Start development

```
# npm start
pnpm run dev
```

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>

### Build

```
pnpm run build
```

Builds the app for production to the `dist` folder.<br>
It correctly bundles Solid in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
Your app is ready to be deployed!

### Preview

After running `npm run build` you can locally preview the application for production build. It serves the
content from the `dist` directory.

```
pnpm run preview
```

## Deployment

You can deploy the `dist` folder to any static host provider (netlify, surge, now, etc.)
