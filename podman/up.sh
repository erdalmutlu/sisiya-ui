#!/usr/bin/env bash
#
# This script start containers in development environment.
#
#####################################################################
podman_up() {
    echo "Starting containers..."

    c="sisiya-ui-nginx"
    if ! podman container exists "$c" ; then
      echo "Starting container [$c] ..."
      podman run -dt --name="$c" --network=host \
        --mount type=bind,src=./nginx/nginx.conf,dst=/etc/nginx/nginx.conf,ro=true \
        --mount type=bind,src=./nginx/conf.d,dst=/etc/nginx/conf.d,ro=true \
        --mount type=bind,src=local_tmp_files/log/nginx,dst=/var/log/nginx \
        localhost/sisiya-ui:latest || exit 1
    fi
}
###############################################################################
cmd=$(basename "$(pwd)")
# echo "You are using #cmd"

if ! which "$cmd" >/dev/null; then
    echo "$0: $cmd is not installed on your computer!"
    exit 1
fi

MY_UID=$(id -u)
MY_GID=$(id -g)
export MY_UID MY_GID
# echo "MY_UID=$MY_UID MY_GID=$MY_GID"

make create_volume

mkdir -p local_tmp_files/log/nginx

case "$cmd" in
    docker)
        docker compose -f docker-compose.yml -f docker-compose-dev.yml up -d "$@"
        ;;
    podman)
        podman_up
        ;;
    *)
        echo "Unknown container software: $cmd"
        ;;
esac

make ps