#!/usr/bin/env bash
#
# This script start podman in development environment.
#
#####################################################################
cmd=$(basename "$(pwd)")
# echo "You are using #cmd"

if ! which "$cmd" >/dev/null; then
    echo "$0: $cmd is not installed on your computer!"
    exit 1
fi

podman_down() {
  c="sisiya-ui-nginx"
  echo "Checking container [$c]..."
  if podman container exists "$c" ; then
    echo podman stop "$c"
    podman stop "$c"
  
    echo podman rm "$c"
    podman rm "$c"
  fi
}

case "$cmd" in
    docker)
        docker compose down
        ;;
    podman)
        podman_down
        ;;
    *)
        echo "Unknown container software: $cmd"
        ;;
esac

make ps
