# Prerequisites for local testing

- Add alias for localhost:

```
sudo echo "127.0.0.1 sisiya-ui.localdomain" >> /etc/hosts
```

# URLs

## Swagger UI

```
http://sisiya-ui.localdomain:3000
```

# Building container image and using the application

## Build container image

```
make build
```

## Start containers

```
make up
```

## Stop containers

```
make down
```

## Show containers

```
make ps
```

## Scan the live image with trivy

```
make scan
```

## Statistics of running containers

```
make stats
```
