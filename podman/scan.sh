#!/usr/bin/env bash
#
# This script scans container images for valnurabilities.
# For this to work enable podman.socket.
# systemctl --user enable --now podman.socket
#
###########################################################################
### defaults
conf_file=image.conf
trivy_cache_dir="/var/tmp/trivy_cache"
number_of_images=0
project_name=project1
image_names[0]="${project_name}"
image_versions[0]="__FROM_GIT__"
### end of defaults
###########################################################################
cmd=$(basename "$(pwd)")
# echo "You are using #cmd"

if ! which "$cmd" >/dev/null; then
    echo "$0: $cmd is not installed on your computer!"
    exit 1
fi

if [[ ! -f $conf_file ]]; then
	echo "$0: image.conf file does not exist!"
	exit 1
fi

# source the image conf
# shellcheck disable=SC1090
source $conf_file
 
version_str=$(git describe)
user_id=$(id -u)

mkdir -p "$trivy_cache_dir"

declare -i i=0
while [ $i -lt $number_of_images ]; do
	s="${image_names[$i]}"
	v="${image_versions[$i]}"
	if [ "$v" = "__FROM_GIT__" ]; then
		v=$version_str
	fi
	echo "$0: Checking image $s:$v..."
	if ! "$cmd" image ls | grep "$s" | grep "$v" ; then
		echo "$0: Container image $s:$v does not exist! Please build it first."
	else
		echo "$0: Scanning image $s:$v..."

		socket_file="/var/run/docker.sock"
		if [[ "$cmd" == "podman" ]]; then
			socket_file="/run/user/${user_id}/podman/podman.sock"
		fi
		
		"$cmd" run --rm \
			--mount "type=bind,src=${trivy_cache_dir},dst=/root/.cache/" \
			--mount "type=bind,src=${socket_file},dst=/var/run/docker.sock" \
			docker.io/aquasec/trivy:latest image --exit-code 1 --severity HIGH,CRITICAL --no-progress "$s:$v"
	fi
	i=$((i+1))
done
